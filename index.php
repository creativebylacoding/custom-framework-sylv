<?php

//oncharge les fichier d'init + vendor
require 'core/app.php';
require 'vendor/autoload.php';
require 'load.php';
$app = new Init();

//verifier si index ou autre URL
if (!isset($_SERVER['PATH_INFO'])){
  //rien dans l'url, donc index
  //on charge le modèle et le controlleur
  require_once  __DIR__.'/Models/indexModel.php';
  require_once  __DIR__.'/Controllers/indexController.php';

  $app->model = new IndexModel();
  $app->controller = new IndexController($app->model);
  //on appelle la methode welcome
  print $app->controller->welcome();
}
else {
  //on parse l'URL
  $request = (explode('/', $_SERVER['PATH_INFO']));
  //on require les fichiers nécessaires
  if(file_exists(__DIR__.'/Controllers/'.ucfirst($request[1])."Controller.php")){
    require_once  __DIR__.'/Models/'.ucfirst($request[1])."Model.php";
    require_once  __DIR__.'/Controllers/'.ucfirst($request[1])."Controller.php";
    // ex= UserModel
    $model = ucfirst($request[1])."Model";
    //on instancie le modèle
    $app->model = new $model;
    // ex= UserController
    $ctrl = ucfirst($request[1])."Controller";
    //on instancie le ctrl avec le modèle en paramètre
    $app->controller = new $ctrl($app->model);

    //on verifie si une action est demandée, sinon action index()
    if (!isset($request[2])){
      print $app->controller->index();
    }
    else {
      $action = $request[2];
      print $app->controller->$action();
    }
  }
  else{
    require_once  __DIR__.'/Models/ErrorModel.php';
    require_once  __DIR__.'/Controllers/ErrorController.php';
    header('HTTP/1.1 404 Not Found');
    $errorModel = new ErrorModel();
    $errorHandler = new ErrorController($errorModel);
    $errorHandler->showError('404');
    //die("404 - Le controlleur ".ucfirst($request[1])."Controller n'existe pas");
  }
}
// echo "Controller $ctrl <br>";
// echo "Controller $action <br>";
?>
