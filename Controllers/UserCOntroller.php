<?php
  class UserController {
    private $model;

    function __construct($obj){
      $this->model = new $obj;
    }

    function index(){
      $data = [];
      //REQUETE SQL;
      $data['fullname'] = "Toto titi";
      $data['points'] = "1500";
      $data['commandes'] =  [
        "1er janvier / 300 € / 15 Tacos XL",
        "1er février / 30 € / Tacos XXL",
        "1er mars / 5 € / Coca "
      ];
      Init::view('user/index', $data);
    }

    function points(){
      print "Vous avez 100 points";
    }
  }
 ?>
